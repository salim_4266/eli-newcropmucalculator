﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Configuration;


namespace NewCropMUWindowsService
{
    public partial class NewCropScheduler : ServiceBase
    {
        private Timer newCropTimer = null;
        private static string sqlConString;

        public NewCropScheduler()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            newCropTimer = new Timer();
            this.newCropTimer.Interval = 1800000; // 30 minutes !
            this.newCropTimer.Elapsed += new ElapsedEventHandler (this.newCropTimer_Tick);
            newCropTimer.Enabled = true;
        }

        protected override void OnStop()
        {
            newCropTimer.Enabled = false;
        }

        private void newCropTimer_Tick (object sender, ElapsedEventArgs e)
        {
            try
            {
                sqlConString = ConfigurationSettings.AppSettings["sqlConString"].ToString();
                ServiceLibrary.SetupRepository(sqlConString);
                DataTable prescriberID = ServiceLibrary.GetNewCropPrescriberID(sqlConString);

                foreach (DataRow row in prescriberID.Rows)
                {
                    string licensedPrescriberID = row[0].ToString();
                    ServiceLibrary.GetDailyMeaningfulUseReport(licensedPrescriberID);
                }

            }
            catch (Exception ex)
            {
                ServiceLibrary.LogException(ex.Message, ex.InnerException.ToString());
            }
        }
    }
}
