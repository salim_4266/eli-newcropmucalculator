﻿using System;
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.IO.Compression;
using NewCropMUWindowsService.com.newcropaccounts.secure;

namespace NewCropMUWindowsService
{
    public static class ServiceLibrary
    {
        private static string sqlConString;
        private static string newCropClientConfigurationXml;
        private static NewCropClientConfiguration newCropClientConfiguration;


        public static void LogException(string message, string innerException)
        {
            sqlConString = ConfigurationSettings.AppSettings["sqlConString"].ToString();
            using (var connection = new SqlConnection(sqlConString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    const string sql = @"
                                        INSERT INTO [model].[NewCropDailyMeaningfulUseLogs]
                                            ([Message]
                                            ,[InnerException]
                                            ,[LogDateTime])
                                        VALUES
                                            (@message
                                            ,@innerException
                                            ,GETDATE())";

                    command.CommandText = sql;

                    command.Parameters.Add(new SqlParameter("message", message));
                    command.Parameters.Add(new SqlParameter("innerException", innerException));
                    command.ExecuteNonQuery();
                }
            }
        }

        public static DataTable GetNewCropPrescriberID(string sqlConString)
        {
            DataTable table = new DataTable();
            using (var connection = new SqlConnection(sqlConString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT ResourceId FROM resources WHERE ResourceType = 'D'", connection))

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    adapter.Fill(table);
                }
            }
            return table;
        }

        private static NewCropClientConfiguration DeserializeClientConfiguration(string value)
        {
            using (var sr = new StringReader(value))
            using (XmlReader xr = XmlReader.Create(sr))
            {
                xr.Read();
                return new NewCropClientConfiguration
                {
                    Update1ServiceUrl = xr.GetAttribute("update1ServiceUrl"),
                    PartnerName = xr.GetAttribute("partnerName"),
                    UserName = xr.GetAttribute("userName"),
                    Password = xr.GetAttribute("password"),
                    AccountId = xr.GetAttribute("accountId"),
                    SiteId = xr.GetAttribute("siteId")
                };
            }
        }


        private static NewCropClientConfiguration GetNewCropClientConfiguration(string sqlConString)
        {
            using (var connection = new SqlConnection(sqlConString))
            {
                connection.Open();

                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT TOP 1 VALUE FROM model.ApplicationSettings WHERE Name = 'NewCropClientConfiguration'";
                    var value = command.ExecuteScalar() as string;

                    if (value == null) throw new Exception("NewCropClientConfiguration not found.");

                    if (newCropClientConfigurationXml != value || newCropClientConfiguration == null)
                    {
                        newCropClientConfigurationXml = value;
                        newCropClientConfiguration = DeserializeClientConfiguration(value);
                    }
                }
            }
            return newCropClientConfiguration;
        }

        private static Update1 GetUpdate1Service(NewCropClientConfiguration configuration)
        {
            var service = new Update1();
            service.Url = configuration.Update1ServiceUrl;
            return service;
        }

        private static Credentials GetCredentials(NewCropClientConfiguration configuration)
        {
            return new Credentials
            {
                Name = configuration.UserName,
                PartnerName = configuration.PartnerName,
                Password = configuration.Password
            };
        }

        private static AccountRequest GetAccountRequest(NewCropClientConfiguration configuration)
        {
            return new AccountRequest
            {
                AccountId = configuration.AccountId,
                SiteId = configuration.SiteId
            };
        }

        public static void SetupRepository(string sqlConString)
        {
            using (var connection = new SqlConnection(sqlConString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    const string sql = @"
                                    IF OBJECT_ID(N'model.NewCropDailyMeaningfulUseReports', N'U') IS NULL
                                    BEGIN
                                        CREATE TABLE [model].[NewCropDailyMeaningfulUseReports](
                                        [SourceTypeID] [int] NULL,
                                        [ActionTypeID] [int] NULL,
                                        [DoctorExternalID] [int] NULL,
                                        [LocationExternalID] [int] NULL,
                                        [PerformerUserTypeID] [int] NULL,
                                        [DocumentMimeType] [varchar](200) NULL,
                                        [DocumentClassificationType] [varchar](200) NULL,
                                        [DocumentClassificationSubType] [varchar](400) NULL,
                                        [DestinationTypeID] [int] NULL,
                                        [ReconciliationPerformed] [bit] NULL,
                                        [EpisodeID] [int] NULL,
                                        [EncounterID] [int] NULL,
                                        [CreatedTimestamp] [datetime] NULL
                                        ) ON [PRIMARY]
                                    END

                                    IF OBJECT_ID(N'model.NewCropDailyMeaningfulUseLogs', N'U') IS NULL
                                    BEGIN
                                        CREATE TABLE [model].[NewCropDailyMeaningfulUseLogs](
                                        [LogID] [int] IDENTITY(1,1) NOT NULL,
                                        [Message] [varchar](max) NULL,
                                        [InnerException] [varchar](max) NULL,
                                        [LogDateTime] [datetime] NULL
                                        ) ON [PRIMARY] 
                                    END ";
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void GetDailyMeaningfulUseReport(string licensedPrescribedId)
        {
            NewCropClientConfiguration configuration = GetNewCropClientConfiguration(sqlConString);
            Update1 service = GetUpdate1Service(configuration);
            var response = service.GetDailyMeaningfulUseReport(GetCredentials(configuration), GetAccountRequest(configuration), configuration.SiteId, licensedPrescribedId, DateTime.Now.ToString("yyyyMMdd"), null);
            byte[] data = Convert.FromBase64String(response.XmlResponse.ToString());
            if (data.Length > 0)
            {
                var responseString = Encoding.Default.GetString(data);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseString.ToString());
                XmlNodeList xnList = xmlDoc.SelectNodes("NewDataSet/Table");

                foreach (XmlNode node in xnList)
                {
                    NewCropClientResults newCropClientResults = new NewCropClientResults();
                    newCropClientResults.SourceTypeID = node["SourceTypeID"].InnerText;
                    newCropClientResults.ActionTypeID = node["ActionTypeID"].InnerText;
                    newCropClientResults.DoctorExternalID = node["DoctorExternalID"].InnerText;
                    newCropClientResults.LocationExternalID = node["LocationExternalID"].InnerText;
                    newCropClientResults.PerformerUserTypeID = node["PerformerUserTypeID"].InnerText;
                    newCropClientResults.DocumentMimeType = node["DocumentMimeType"].InnerText;
                    newCropClientResults.DocumentClassificationType = node["DocumentClassificationType"].InnerText;
                    newCropClientResults.DocumentClassificationSubType = node["DocumentClassificationSubType"].InnerText;
                    newCropClientResults.DestinationTypeID = node["DestinationTypeID"].InnerText;
                    newCropClientResults.ReconciliationPerformed = node["ReconciliationPerformed"].InnerText;
                    newCropClientResults.EpisodeID = node["EpisodeID"].InnerText;
                    newCropClientResults.EncounterID = node["EncounterID"].InnerText;
                    newCropClientResults.CreatedTimestamp = node["CreatedTimestamp"].InnerText;

                    StoreParametersAndResult(newCropClientResults);
                }
            }
        }



        private static void StoreParametersAndResult(NewCropClientResults newCropClientResults)
        {
            using (var connection = new SqlConnection(sqlConString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    const string sql = @"
                                        IF NOT EXISTS (SELECT * FROM [model].[NewCropDailyMeaningfulUseReports] 
                                        WHERE SourceTypeID = @sourceTypeID
                                        AND ActionTypeID = @actionTypeID
                                        AND DoctorExternalID = @doctorExternalID
                                        AND LocationExternalID = @locationExternalID
                                        AND PerformerUserTypeID = @performerUserTypeID
                                        AND DocumentMimeType = @documentMimeType
                                        AND DocumentClassificationType = @documentClassificationType
                                        AND DocumentClassificationSubType = @documentClassificationSubType
                                        AND DestinationTypeID = @destinationTypeID
                                        AND ReconciliationPerformed = @reconciliationPerformed
                                        AND EpisodeID = @episodeID
                                        AND EncounterID = @encounterID
                                        AND CreatedTimestamp = @createdTimestamp)
                                        BEGIN
                                             INSERT INTO [model].[NewCropDailyMeaningfulUseReports]
                                                   ([SourceTypeID]
                                                   ,[ActionTypeID]
                                                   ,[DoctorExternalID]
                                                   ,[LocationExternalID]
                                                   ,[PerformerUserTypeID]
                                                   ,[DocumentMimeType]
                                                   ,[DocumentClassificationType]
                                                   ,[DocumentClassificationSubType]
                                                   ,[DestinationTypeID]
                                                   ,[ReconciliationPerformed]
                                                   ,[EpisodeID]
                                                   ,[EncounterID]
                                                   ,[CreatedTimestamp])
                                             VALUES
                                                   (@sourceTypeID
                                                   ,@actionTypeID
                                                   ,@doctorExternalID
                                                   ,@locationExternalID
                                                   ,@performerUserTypeID
                                                   ,@documentMimeType
                                                   ,@documentClassificationType
                                                   ,@documentClassificationSubType
                                                   ,@destinationTypeID
                                                   ,@reconciliationPerformed
                                                   ,@episodeID
                                                   ,@encounterID
                                                   ,@createdTimestamp)
                                        END ";

                    command.CommandText = sql;

                    command.Parameters.Add(new SqlParameter("sourceTypeID", newCropClientResults.SourceTypeID));
                    command.Parameters.Add(new SqlParameter("actionTypeID", newCropClientResults.ActionTypeID));
                    command.Parameters.Add(new SqlParameter("doctorExternalID", newCropClientResults.DoctorExternalID));
                    command.Parameters.Add(new SqlParameter("locationExternalID", newCropClientResults.LocationExternalID));
                    command.Parameters.Add(new SqlParameter("performerUserTypeID", newCropClientResults.PerformerUserTypeID));
                    command.Parameters.Add(new SqlParameter("documentMimeType", newCropClientResults.DocumentMimeType));
                    command.Parameters.Add(new SqlParameter("documentClassificationType", newCropClientResults.DocumentClassificationType));
                    command.Parameters.Add(new SqlParameter("documentClassificationSubType", newCropClientResults.DocumentClassificationSubType));
                    command.Parameters.Add(new SqlParameter("destinationTypeID", newCropClientResults.DestinationTypeID));
                    command.Parameters.Add(new SqlParameter("reconciliationPerformed", newCropClientResults.ReconciliationPerformed));
                    command.Parameters.Add(new SqlParameter("episodeID", newCropClientResults.EpisodeID));
                    command.Parameters.Add(new SqlParameter("encounterID", newCropClientResults.EncounterID));
                    command.Parameters.Add(new SqlParameter("createdTimestamp", newCropClientResults.CreatedTimestamp));

                    command.ExecuteNonQuery();
                }
            }
        }

    }

    [XmlRoot("newCropClientConfiguration")]
    [XmlType]
    [Serializable]
    public class NewCropClientConfiguration
    {
        [XmlAttribute("update1ServiceUrl")]
        public string Update1ServiceUrl { get; set; }

        [XmlAttribute("partnerName")]
        public string PartnerName { get; set; }

        [XmlAttribute("userName")]
        public string UserName { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }

        [XmlAttribute("accountId")]
        public string AccountId { get; set; }

        [XmlAttribute("siteId")]
        public string SiteId { get; set; }
    }

    [XmlRoot("newCropClientResults")]
    [XmlType]
    [Serializable]
    public class NewCropClientResults
    {
        [XmlAttribute("sourceTypeID")]
        public string SourceTypeID { get; set; }

        [XmlAttribute("actionTypeID")]
        public string ActionTypeID { get; set; }

        [XmlAttribute("doctorExternalID")]
        public string DoctorExternalID { get; set; }

        [XmlAttribute("locationExternalID")]
        public string LocationExternalID { get; set; }

        [XmlAttribute("performerUserTypeID")]
        public string PerformerUserTypeID { get; set; }

        [XmlAttribute("documentMimeType")]
        public string DocumentMimeType { get; set; }

        [XmlAttribute("documentClassificationType")]
        public string DocumentClassificationType { get; set; }

        [XmlAttribute("documentClassificationSubType")]
        public string DocumentClassificationSubType { get; set; }

        [XmlAttribute("destinationTypeID")]
        public string DestinationTypeID { get; set; }

        [XmlAttribute("reconciliationPerformed")]
        public string ReconciliationPerformed { get; set; }

        [XmlAttribute("episodeID")]
        public string EpisodeID { get; set; }

        [XmlAttribute("encounterID")]
        public string EncounterID { get; set; }

        [XmlAttribute("createdTimestamp")]
        public string CreatedTimestamp { get; set; }
    }
}
